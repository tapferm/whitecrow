<?php get_header(); ?>


<div id="logo-banner">
    <div>
        <img src="<?php bloginfo('template_directory'); ?>/imgs/logo.svg" >
    </div>
</div>

<div class="main-container focus-pics">

  <!-- SHOP  viimane lisatud toode-->
  <a href="http://whitecrow.local/shop/">
    <div class="focus">
    <h1 class="pic-h1">Pood</h1>
    <?php
        $query2 = new WP_Query(array('post_type' => 'product', 'posts_per_page' => 1));

        if ($query2->have_posts()) : while ($query2->have_posts()) : $query2->the_post(); ?>
                <?php
                if (has_post_thumbnail()) {
                    the_post_thumbnail('front-page');
                };
                ?>
        <?php endwhile;
        else : endif;
        wp_reset_postdata();
        ?>
    </div>
</a>
     <!-- NEWS - viimase blogipostituse pilt-->
    <a href="http://whitecrow.local/category/news/"> 
    <div class="focus">
      <h1 class="pic-h1">Uudised</h1>
     
      <?php
        $query = new WP_Query(array('category_name' => 'uudised', 'posts_per_page' => 1));

        if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                <?php
                if (has_post_thumbnail()) {
                    the_post_thumbnail('front-page');
                };
                ?>
        <?php endwhile;
        else : endif;
        wp_reset_postdata();
        ?>
    </div>
 </a>

    <!-- ABOUT -->
    <a href="http://whitecrow.local/about/">
    <div class="focus">
      <?php
        $query2 = new WP_Query(array('pagename' => 'about', 'posts_per_page' => 1));

        if ($query2->have_posts()) : while ($query2->have_posts()) : $query2->the_post(); ?>
                <h1 class="pic-h1"><?php the_title(); ?></h1>
                <?php
                if (has_post_thumbnail()) {
                    the_post_thumbnail('front-page');
                };
                ?>
        <?php endwhile;
        else : endif;
        wp_reset_postdata();
        ?>
    </div>
</a>

</div>

   
    <?php get_footer(); ?>