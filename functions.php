<?php


function load_stylesheets()
{

    wp_register_style('mystyle', get_template_directory_uri() . '/src/style.css', '', 1, 'all');
    wp_enqueue_style('mystyle');

}
add_action('wp_enqueue_scripts', 'load_stylesheets');



function load_javascript()
{

    wp_register_script('custom', get_template_directory_uri() .  '/app.js', '', '', true);
    wp_enqueue_script('custom');
}
add_action('wp_enqueue_scripts', 'load_javascript');

//add menus
add_theme_support('menus');

add_theme_support('post-thumbnails'); //featured images

//adding menu locations
register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme'),
    )
);



//WIDGETS
register_sidebar(
    array(
        'name' => 'Page Sidebar',
        'id' => 'page-sidebar',
        'class' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    )
);


function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

//IMAGE SIZES
add_image_size('smallest', 100, 100, false); //true == hard crop means dimensions may change
add_image_size('largest', 300, 300, true);
// add_image_size('front-page', 281, 455, TRUE);
add_image_size('front-page', 450, 729, TRUE);
// add_image_size('featured-img', 281, 174, TRUE);
add_image_size('featured-img', 450, 278, true);


add_filter( 'image_size_names_choose', 'my_custom_sizes' );
function my_custom_sizes( $sizes ) {
return array_merge( $sizes, array(
'homepage-thumb' => __( 'Homepage Thumb' ),
'search-thumb' => __( 'Search Thumb' ),
) );
}


?>