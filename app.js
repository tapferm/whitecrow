function showMenu (){
  let menu = document.querySelector(".top-menu");
  menu.classList.toggle("hidden");
  let hamburger= document.querySelector("#hamburger");
  hamburger.classList.toggle("cross");
  hamburger.classList.toggle("nocross");
}
