<?php get_header();?>


<div class="main-container page-contents">
    <!-- <p>siia saan lisada erinevaid asju mis on ainut PAGE-idel nähtav, aga mitte esilehel</p> -->

 <!-- TITLE -->
<h1><?php the_title();?></h1>


<!-- FEATURED IMAGE -->
<?php if(has_post_thumbnail()): ?>
<img class= "featured-img" src="<?php the_post_thumbnail_url('featured-img'); ?>">
<?php endif; ?>

<!-- CONTENTS -->
<?php if (have_posts()) : while(have_posts()) : the_post(); ?>
    <?php the_content (); ?>
<?php endwhile; else: endif; ?>


<!-- WIDGET ON SIDE -->
<!-- <?php get_sidebar();?> -->

</div>


<?php get_footer();?>