<?php get_header(); ?>
/*
Template Name: Shop Page
*/


<div class="main-container">
    <!-- TITLE -->
    <h1><?php the_title(); ?></h1>


    <!-- CONTENTS -->
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
    <?php endwhile;
    else : endif; ?>
</div>


<?php get_footer(); ?>