<?php get_header();?>


<div class="main-container">
    <p>siia saan lisada asju mis on ainut Blogipostituste kuvamisel nähtavad-idel nähtav, aga mitte esilehel</p>
<!-- TITLE -->
<h1><?php the_title();?></h1>

<!-- FEATURED IMAGE -->
<?php if(has_post_thumbnail()): ?>
<img src="<?php the_post_thumbnail_url('largest'); ?>" >
<?php endif; ?>

<!-- BLOG CONTENTS -->
<?php if (have_posts()) : while(have_posts()) : the_post(); ?>
    <?php the_content (); ?>
<?php endwhile; endif; ?>


</div>


<?php get_footer();?>