<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>White Crow design</title>
<?php wp_head();?>

</head>


<body <?php body_class();?>>


<header>
    <div class="nav-bar">
    <img class="top-logo" src="<?php bloginfo('template_directory'); ?>/imgs/logo-top.svg" >
    <svg  class ="nocross" id="hamburger" onclick="showMenu()" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect id="bar3" y="10" x="10" width="30" height="2" fill="#F3F4F4"/>
            <rect id="bar2" y="24" x="10" width="30" height="2" fill="#F3F4F4"/>
            <rect id="bar1" y="38" x="10" width="30" height="2" fill="#F3F4F4"/>
    </svg>
    </div>

    <nav>
                <?php wp_nav_menu(
            array(
                'theme_location' => 'top-menu',
                'menu_class' => 'top-menu hidden' //siin saan lisada classe
            )
        ); ?> 
    </nav>

</header>






    